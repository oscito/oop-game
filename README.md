# OOP game

## Weapons
Every weapon has these characteristics:

- Minimum damage
- Maximum damage
- Name

### Excalibur
Excalibur is a legendary sword of King Arthur. It can devastate even the strongest opponents with its precise strikes.

| Property       | Value     |
|----------------|-----------|
| Name           | Excalibur |
| Minimum damage | 100       |
| Maximum damage | 150       |

### Pickaxe
Pickaxe is a hand tool, that was mainly used for agriculture. However, people realized that it can be used as a weapon.

| Property       | Value     |
|----------------|-----------|
| Name           | Pickaxe   |
| Minimum damage | 50        |
| Maximum damage | 100       |

### Magical Scepter
Magical Scepter was used by wizards to protect human nation from aliens. If used correctly, it can deal insane amounts of damage.
However, usage is so hard, so damage is pretty unpredictable.

| Property       | Value           |
|----------------|-----------------|
| Name           | Magical Scepter |
| Minimum damage | 10              |
| Maximum damage | 500             |

## Armors
Armors are used to reduce incoming damage.
Every armor has these characteristics:

- Name
- Amount

### Cloth armor
Warriors, that are from poor families, opt to cloth armor for its price, but it doesn't do that much.

| Property | Value           |
|----------|-----------------|
| Name     | Cloth armor     |
| Amount   | 10              |


### Chain vest
Knights have luxury of owning vest full of chains. It can block almost any attack.

| Property       | Value           |
|----------------|-----------------|
| Name           | Cloth armor     |
| Amount         | 50              |

### Nano suit
Nano suit disrupted war industry. Scientists have created very thin and strong suit.

| Property       | Value           |
|----------------|-----------------|
| Name           | Nano suit       |
| Amount         | 150             |

damage multiplier = 100/(100 + armor)