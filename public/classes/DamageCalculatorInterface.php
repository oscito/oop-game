<?php

// this interface is temporary because no good solution is found yet
interface DamageCalculatorInterface
{
    public function calculate(WeaponInterface $weapon, ArmorInterface $armor): float;
}