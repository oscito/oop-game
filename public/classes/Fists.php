<?php

class Fists implements WeaponInterface
{
    public function getMinDamage(): float
    {
        return mt_rand(0, 5);
    }

    public function getMaxDamage(): float
    {
        return mt_rand(5, 2000);
    }

    public function getName(): string
    {
        return "Golden Fist";
    }

    public function getCriticalChance(): float
    {
        return 100;
    }
}