<?php

// this class is temporary because no good solution is found yet
class CriticalDamageCalculator implements DamageCalculatorInterface
{

    public function calculate(WeaponInterface $weapon, ArmorInterface $armor): float
    {
        $damage = mt_rand($weapon->getMinDamage(), $weapon->getMaxDamage());
        $coefficient = 100/(100+$armor->getAmount());

        if (mt_rand(0, 100) < $weapon->getCriticalChance()) {
            $damage *= 2;
            $this->isCritical = true;
        }

        return $damage * $coefficient;
    }
}