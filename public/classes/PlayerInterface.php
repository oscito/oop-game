<?php

interface PlayerInterface
{
    public function getWeapon(): WeaponInterface;
    public function getArmor(): ArmorInterface;
    public function getName(): string;
    public function getHealth(): float;
    public function reduceHealth(float $amount);
}