<?php

interface WeaponInterface
{
    public function getMinDamage(): float;
    public function getMaxDamage(): float;
    public function getName(): string;
    public function getCriticalChance(): float;
}