<?php

class Excalibur extends StaticValuesWeapon {
    protected $minDamage = 100;
    protected $maxDamage = 150;
    protected $name = 'Excalibur';
}
