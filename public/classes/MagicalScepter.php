<?php

class MagicalScepter extends StaticValuesWeapon
{
    protected $minDamage = 10;
    protected $maxDamage = 500;
    protected $name = 'Magical Scepter';
    protected $criticalChance = 90;
}