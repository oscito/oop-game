<?php

class Game
{
    protected $player1;
    protected $player2;
    protected $isCritical = false;

    public function __construct(PlayerInterface $player1, PlayerInterface $player2)
    {
        $this->player1 = $player1;
        $this->player2 = $player2;
    }

    public function start()
    {
        while ($this->player1->getHealth() > 0 && $this->player2->getHealth() > 0) {
            $this->attack($this->player1, $this->player2);

            if ($this->player2->getHealth() > 0) {
                $this->attack($this->player2, $this->player1);
            }
        }

        $this->declareWinner();
    }

    protected function calculateDamage(WeaponInterface $weapon, ArmorInterface $armor)
    {
        $damage = mt_rand($weapon->getMinDamage(), $weapon->getMaxDamage());
        $coefficient = 100/(100+$armor->getAmount());

        if (mt_rand(0, 100) < $weapon->getCriticalChance()) {
            $damage *= 2;
            $this->isCritical = true;
        }

        return $damage * $coefficient;
    }

    protected function attack(PlayerInterface $attacker, PlayerInterface $defender) {
        $this->isCritical = false;
        $damage = $this->calculateDamage($attacker->getWeapon(), $defender->getArmor());
        $defender->reduceHealth($damage);

        echo $attacker->getName() .' hit for ';

        if ($this->isCritical) {
            echo '<b>'. $damage .'</b>';
        } else {
            echo $damage;
        }

        echo ' damage leaving his opponent '. $defender->getHealth() .' HP<br>';
    }

    protected function declareWinner()
    {
        if ($this->player1->getHealth() == 0) {
            echo $this->player2->getName() . ' has defeated his opponent<br>';
        } else {
            echo $this->player1->getName() . ' has defeated his opponent<br>';
        }
    }
}