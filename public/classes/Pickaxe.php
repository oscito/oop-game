<?php

class Pickaxe extends StaticValuesWeapon
{
    protected $minDamage = 50;
    protected $maxDamage = 100;
    protected $name = 'Pickaxe';
}