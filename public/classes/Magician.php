<?php

class Magician implements PlayerInterface
{
    protected $health = 50;

    public function getWeapon(): WeaponInterface
    {
        return new MagicalScepter();
    }

    public function getArmor(): ArmorInterface
    {
        return new ClothArmor();
    }

    public function getName(): string
    {
        return 'Magician';
    }

    public function getHealth(): float
    {
        return $this->health;
    }

    public function reduceHealth(float $amount)
    {
        $this->health += $amount;
    }
}