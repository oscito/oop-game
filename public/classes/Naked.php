<?php

class Naked implements ArmorInterface
{

    public function getAmount(): int
    {
        return 50;
    }

    public function getName(): string
    {
        return 'Naked';
    }
}