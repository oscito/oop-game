<?php

spl_autoload_register(function ($class) {
    require_once 'classes/'. $class .'.php';
});

$excalibur = new Fists();
//$nanoSuit = new NanoSuit();
$armor = new Naked();
$player1 = new Player('Jonas', 2000, $excalibur, $armor);

$magicScepter = new MagicalScepter();
$clothArmor = new ClothArmor();
$player2 = new Player('Petras', 1500, $magicScepter, $clothArmor);

$magician = new Magician();

$game = new Game($player1, $magician);

$game->start();